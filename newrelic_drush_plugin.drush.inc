<?php

/*
 * @file
 *   Reports to new relic as a background job.
 */

/**
 * Implements of hook_drush_init().
 */
function newrelic_drush_plugin_drush_init() {
  $command = drush_get_command();
  if ($command && $command != 'help') {
    if (extension_loaded('newrelic')) {
      ini_set('newrelic.framework', 'drupal');
      $context = drush_get_context();
      $uri = '';
      if (isset($context['DRUSH_URI'])) {
        $uri = $context['DRUSH_URI'];
        $uri = str_replace('http://', '', $uri);
        $uri = str_replace('https://', '', $uri);
      }
      else if (isset($context['DRUSH_DRUPAL_SITE'])) {
        $uri = $context['DRUSH_DRUPAL_SITE'];
      }

      if (!empty($uri)) {
        $this_instance = 'Site: ' . $uri;

        // Support barracuda, octopus naming convention.
        if (isset($context['DRUSH_DRUPAL_CORE'])) {
          $root = $context['DRUSH_DRUPAL_CORE'];
        }
        elseif (isset($context['DRUSH_DRUPAL_ROOT'])) {
          $root = $context['DRUSH_DRUPAL_ROOT'];
        }

        if (!empty($root)) {
          $frags = explode("/", $root);
          if ($frags[1] == 'data') {
            $this_instance = 'Site: ' . $uri . ';AAA Octopus ' . $frags[3] . ';AAA All Instances';
          }
          elseif ($frags[2] == 'aegir') {
            $this_instance = 'Site: ' . $uri . ';AAA Barracuda Master;AAA All Instances';
          }
        }
        ini_set('newrelic.appname', $this_instance);
        newrelic_set_appname($this_instance);

        // Build drush command.
        $drush_command = array_merge(array($command['command']), $command['arguments']);
        newrelic_add_custom_parameter('Drush command', implode(' ', $drush_command));
        newrelic_name_transaction(implode(' ', $drush_command));
        newrelic_background_job(TRUE);
      }
    }
  }
}
